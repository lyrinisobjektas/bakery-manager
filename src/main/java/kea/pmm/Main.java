package kea.pmm;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import kea.pmm.controller.SaleTabController;
import kea.pmm.controller.ScreenChanger;
import kea.pmm.dao.ProductDao;
import kea.pmm.entity.Employee;
import kea.pmm.entity.Product;
import kea.pmm.utility.HibernateUtil;
import kea.pmm.utility.ObjectLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        // Loads objects from the database that won't change during the runtime
        ObjectLoader.loadObjectsFromDb();

        primaryStage.setTitle("Enli5 POS");
        primaryStage.setScene(new Scene(loadMainPane()));
        primaryStage.show();

        // Close hibernate connection to db on exit
        primaryStage.setOnCloseRequest(event -> {
            HibernateUtil.getSessionFactory().close();
        });

    }

    private BorderPane loadMainPane() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(ScreenChanger.MAIN_PANE));
        BorderPane mainPane = loader.load();
        return mainPane;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
