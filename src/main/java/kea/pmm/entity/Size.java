package kea.pmm.entity;

import javax.persistence.*;

@Entity
@Table(name = "Sizes")
public class Size {

    private int sizeId;
    private String sizeLetter;

    @Id
    @Column(name = "sizeId", nullable = false, insertable = true, updatable = true)
    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    @Basic
    @Column(name = "sizeLetter", nullable = false, insertable = true, updatable = true, length = 6)
    public String getSizeLetter() {
        return sizeLetter;
    }

    public void setSizeLetter(String sizeLetter) {
        this.sizeLetter = sizeLetter;
    }

    @Override
    public String toString() {
        return getSizeLetter();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Size size = (Size) o;

        if (sizeId != size.sizeId) return false;
        if (sizeLetter != null ? !sizeLetter.equals(size.sizeLetter) : size.sizeLetter != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sizeId;
        result = 31 * result + (sizeLetter != null ? sizeLetter.hashCode() : 0);
        return result;
    }
}
