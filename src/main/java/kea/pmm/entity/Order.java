package kea.pmm.entity;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import kea.pmm.utility.LocalDatePersistenceConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Table(name = "Orders")
public class Order {

    private static AtomicInteger uniqueId = new AtomicInteger(0);

    private IntegerProperty orderId;
    private ObservableList<OrderItem> orderItems;
    private ObjectProperty<LocalDateTime> date;
    private Employee employee;
    private ObjectProperty<BigDecimal> totalPrice;
    private BooleanProperty isCompleted;

    public Order() {

        this.orderId = new SimpleIntegerProperty();
        this.totalPrice = new SimpleObjectProperty<>(new BigDecimal(0.00));
        this.date = new SimpleObjectProperty<>(LocalDateTime.now());
        this.isCompleted = new SimpleBooleanProperty(false);
        orderItems = FXCollections.observableArrayList();

        // Recalculates totalcost every time orderItems list is changed (added or removed items)
        orderItems.addListener((ListChangeListener.Change<? extends OrderItem> c) ->{
            System.out.println("order items list changed. recalculating price");
            recalculateTotalCost();
        });
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "orderId", nullable = false, insertable = true, updatable = true)
    public int getOrderId() {
        return orderId.get();
    }

    public void setOrderId(int orderId) {
        this.orderId.set(orderId);
    }

    public IntegerProperty orderIdProperty() {
        return this.orderId;
    }

    @Convert (converter = LocalDatePersistenceConverter.class)
    @Column(name = "date", nullable = false, insertable = true, updatable = true)
    public LocalDateTime getDate() {
        return date.get();
    }

    public void setDate(LocalDateTime date) {
        this.date.set(date);
    }

    public ObjectProperty<LocalDateTime> dateProperty() {
        return this.date;
    }

    @ManyToOne
    @JoinColumn(name = "employeeId")
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Basic
    @Column(name = "totalPrice", nullable = false, insertable = true, updatable = true)
    public BigDecimal getTotalPrice() {
        return totalPrice.get();
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice.set(totalPrice);
    }

    public ObjectProperty<BigDecimal> totalPriceProperty() {
        return this.totalPrice;
    }

    @Transient
    public ObservableList<OrderItem> getOrderItems() {
        return orderItems;
    }

    // CascadeType.ALL persists all associated OrderItems automatically.
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    public List<OrderItem> getOrderItemsList() {
        return orderItems;
    }

    public void setOrderItemsList(List<OrderItem> orderItems) {
        this.orderItems = FXCollections.observableArrayList(orderItems);
    }

    public boolean getIsCompleted() {
        return isCompleted.get();
    }

    @Transient
    public BooleanProperty isCompletedProperty() {
        return isCompleted;
    }

    public void setIsCompleted(boolean isCompleted) {
        this.isCompleted.set(isCompleted);
    }

    private void recalculateTotalCost() {
        this.totalPrice.setValue(new BigDecimal(0.00));
        for (OrderItem orderItem : orderItems) {
            this.totalPrice.setValue(this.totalPrice.get().add(orderItem.getPrice()));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (orderId != order.orderId) return false;
        if (employee != order.employee) return false;
        if (totalPrice != order.totalPrice) return false;
        if (date != null ? !date.equals(order.date) : order.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId.get();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + employee.getEmployeeId();
        result = 31 * result + totalPrice.get().intValue();
        return result;
    }
}
