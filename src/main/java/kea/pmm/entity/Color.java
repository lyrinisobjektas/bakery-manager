package kea.pmm.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Color {

    private int colorId;
    private String colorName;

    @Id
    @Column(name = "colorId", nullable = false, insertable = true, updatable = true)
    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    @Basic
    @Column(name = "colorName", nullable = false, insertable = true, updatable = true, length = 255)
    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    @Override
    public String toString() {
        return getColorName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Color color = (Color) o;

        if (colorId != color.colorId) return false;
        if (colorName != null ? !colorName.equals(color.colorName) : color.colorName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = colorId;
        result = 31 * result + (colorName != null ? colorName.hashCode() : 0);
        return result;
    }
}
