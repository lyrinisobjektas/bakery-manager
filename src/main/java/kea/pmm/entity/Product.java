package kea.pmm.entity;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Product {

    private int productId;
    private ProductDescription productDescription;
    private ObjectProperty<Size> sizeId = new SimpleObjectProperty<>();
    private ObjectProperty<Color> colorId = new SimpleObjectProperty<>();
    private BigDecimal price;
    private int quantity;

    public Product() {
    }

    @Id
    @Column(name = "productId", nullable = false, insertable = true, updatable = true)
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @OneToOne
    @JoinColumn(name = "productDescriptionId")
    public ProductDescription getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(ProductDescription productDescription) {
        this.productDescription = productDescription;
    }

    @OneToOne
    @JoinColumn(name = "sizeId")
//    @Column(name = "sizeId", nullable = true, insertable = true, updatable = true)
    public Size getSizeId() {
        return sizeId.get();
    }

    public void setSizeId(Size sizeId) {
        this.sizeId.set(sizeId);
    }

    public ObjectProperty sizeProperty() {
        return this.sizeId;
    }

    @OneToOne
    @JoinColumn(name = "colorId")
//    @Column(name = "colorId", nullable = false, insertable = true, updatable = true)
    public Color getColorId() {
        return colorId.get();
    }

    public void setColorId(Color colorId) {
        this.colorId.set(colorId);
    }

    public ObjectProperty colorProperty() {
        return this.colorId;
    }

    @Basic
    @Column(name = "price", nullable = false, insertable = true, updatable = true, precision = 2)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "quantity", nullable = false, insertable = true, updatable = true)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productDescriptionId=" + productDescription +
                ", sizeId=" + sizeId +
                ", colorId=" + colorId +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
