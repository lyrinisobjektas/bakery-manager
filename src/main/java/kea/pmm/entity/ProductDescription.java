package kea.pmm.entity;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

import javax.persistence.*;

@Entity
public class ProductDescription {

    private int productDescriptionId;
    private StringProperty name = new SimpleStringProperty();
    private Category categoryId;
    private String imageLink;
    private String description;

    @Id
    @Column(name = "productDescriptionId", nullable = false, insertable = true, updatable = true)
    public int getProductDescriptionId() {
        return productDescriptionId;
    }

    public void setProductDescriptionId(int productDescriptionId) {
        this.productDescriptionId = productDescriptionId;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    @OneToOne
    @JoinColumn(name = "categoryId")
    public Category getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Category categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "imageLink", nullable = true, insertable = true, updatable = true, length = 255)
    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name.get();
    }
}
