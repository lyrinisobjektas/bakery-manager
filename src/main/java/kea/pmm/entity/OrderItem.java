package kea.pmm.entity;

import javafx.beans.property.*;
import javafx.collections.FXCollections;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class OrderItem {

    private int orderItemId;
    private Product productId;
    private Order order;
    private ObjectProperty<BigDecimal> price;
    private IntegerProperty quantity;


    // No-args constructor required by hibernate
    public OrderItem() {
        this.price = new SimpleObjectProperty<>();
        this.quantity = new SimpleIntegerProperty();
    }

    public OrderItem(Product product, int quantity) {
        this.productId = product;
        this.quantity = new SimpleIntegerProperty(quantity);
        this.price = new SimpleObjectProperty<>();
        setPrice(product, quantity);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "orderItemId", nullable = false, insertable = true, updatable = true)
    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    @OneToOne
    @JoinColumn(name = "productId")
    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product product) {
        this.productId = product;
    }

    @ManyToOne
    @JoinColumn(name = "orderId")
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Basic
    @Column(name = "price", nullable = false, insertable = true, updatable = true)
    public BigDecimal getPrice() {
        return price.get();
    }

    public void setPrice(BigDecimal price) {
        this.price.set(price);
    }

    public void setPrice(Product product, int quantity) {
        this.price.set(product.getPrice().multiply(new BigDecimal(quantity)));
    }

    public ObjectProperty priceProperty() {
        return this.price;
    }

    @Basic
    @Column(name = "quantity", nullable = false, insertable = true, updatable = true)
    public int getQuantity() {
        return quantity.get();
    }

    public void setQuantity(int quantity) {
        this.quantity.set(quantity);
    }

    public IntegerProperty quantityProperty() {
        return quantity;
    }

}
