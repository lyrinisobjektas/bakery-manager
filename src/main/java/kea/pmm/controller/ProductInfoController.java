package kea.pmm.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import kea.pmm.entity.*;
import kea.pmm.utility.ObjectLoader;

import java.util.ArrayList;
import java.util.List;

public class ProductInfoController {

    @FXML
    private Node root;

    @FXML
    private Label productNameLabel;

    @FXML
    private Label productDescriptionLabel;

    @FXML
    private Label categoryLabel;

    @FXML
    private Button payButton;

    @FXML
    private Button cancelButton;

    @FXML
    private ComboBox quantityComboBox;

    @FXML
    private ComboBox<Color> colorComboBox;

    @FXML
    private ComboBox sizeComboBox;

    @FXML
    private ImageView productImageHolder;

    @FXML
    private Label quantityLabel;


    //////////////////// FIELDS /////////////////////

    private ProductDescription productDescription;
    private Product selectedProduct;
    private ObservableList<Integer> quantityList;
    private List<Product> matchingProductList;
    private ObservableList<Color> productColorsList;        // List of colors that match product description
    private ObservableList<Size> productSizeList;
    private ObservableMap<Size, ObservableList<Color>> sizeColorMap;

    private SaleTabController saleTabController;


    public ProductInfoController() {
    }

    public void initialize() {
    }

    /**
     * Initializes GUI components instead of using default initialize() method.
     */
    private void initComponents() {

        ////////// BUTTONS //////////

        cancelButton.setOnAction(event -> {
            ((Node) event.getSource()).getScene().getWindow().hide();
        });

        //todo. Need to check if selected product is valid
        payButton.setOnAction(event -> {
            OrderItem orderItem = new OrderItem(getMatchingProduct(), quantityComboBox.getSelectionModel().getSelectedIndex() + 1);
            saleTabController.addOrderItem(orderItem);
            root.getScene().getWindow().hide();
        });

        ////////// LISTS, COMBO BOXES ///////////

        // todo bind color and size combo boxes to be dependant on each other
        sizeComboBox.setItems(FXCollections.observableArrayList(sizeColorMap.keySet()));
        sizeComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue);
            productColorsList.setAll(sizeColorMap.get(newValue));
        });

        colorComboBox.setItems(productColorsList);
        colorComboBox.setOnAction(event -> {
            setSelectedProduct();
        });

        quantityComboBox.setItems(quantityList);
        quantityComboBox.getSelectionModel().select(0);


        //////////////// LABELS ///////////////

        // Image holder
        try {
            Image img = new Image(productDescription.getImageLink());
            productImageHolder.setImage(img);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
            Image noImg = new Image("img/no-image.jpg");
            productImageHolder.setImage(noImg);
        }

        // Product description label
        productDescriptionLabel.setWrapText(true);
        productDescriptionLabel.setText(productDescription.getDescription());

        // Category label
        categoryLabel.setText(productDescription.getCategoryId().getName());

    }

    public void setItem(ProductDescription selectedItem) {
        this.productDescription = selectedItem;
        productNameLabel.setText(selectedItem.getName());

        createMatchingProductList();
        createSizeColorMap();
        makeQuantityList();
        makeProductColorList();
        makeProductSizeList();
        initComponents();
        loadImage();
    }

    private void loadImage() {

    }

    private void makeProductSizeList() {
        productSizeList = FXCollections.observableArrayList();
        for (Product product : matchingProductList) {
            productSizeList.add(product.getSizeId());
        }
    }

    private void createSizeColorMap() {
        sizeColorMap = FXCollections.observableHashMap();
        for (Product product : matchingProductList) {
            if (!sizeColorMap.containsKey(product.getSizeId())) {
                sizeColorMap.put(product.getSizeId(), FXCollections.observableArrayList());
                sizeColorMap.get(product.getSizeId()).add(product.getColorId());
            } else {
                sizeColorMap.get(product.getSizeId()).add(product.getColorId());
            }
        }
        System.out.println(sizeColorMap);
    }

    public void createMatchingProductList() {
        matchingProductList = new ArrayList<>();
        for (Product product : ObjectLoader.getProducts()) {
            if (product.getProductDescription().getName().equals(this.productDescription.getName())) {
                matchingProductList.add(product);
            }
        }
        String text = "Available options:\n\n";
        for (Product product : matchingProductList) {
            text += product.getProductId() + " " + product.getSizeId() + " " + product.getColorId() + "\n";
        }
    }

    public void setSelectedProduct() {
        this.selectedProduct = matchingProductList.get(0);
    }

    private void makeQuantityList() {
        quantityList = FXCollections.observableArrayList();
        for (int i = 1; i < 21; i++) {
            quantityList.add(i);
        }
    }

    private void makeProductColorList() {
        productColorsList = FXCollections.observableArrayList();
        for (Product product : matchingProductList) {
            productColorsList.add(product.getColorId());
        }
    }

    private Product getMatchingProduct() {
        Product matchingProduct = null;
        for (Product product : matchingProductList) {
            if (product.getColorId() == colorComboBox.getSelectionModel().getSelectedItem() &&
                    product.getSizeId() == sizeComboBox.getSelectionModel().getSelectedItem()) {
                matchingProduct = product;
            }
        }
        return matchingProduct;
    }

    public void setSaleTabController(SaleTabController saleTabController) {
        this.saleTabController = saleTabController;
    }

}
