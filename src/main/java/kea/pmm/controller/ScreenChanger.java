package kea.pmm.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;

public class ScreenChanger {

    public static String PAYSCREEN = "/fxml/PayScreen.fxml";
    public static String SALE_TAB = "/fxml/SaleTab.fxml";
    public static String TAB_PANE = "/fxml/TabPane.fxml";
    public static String INFO_WINDOW = "/fxml/ProductInfo.fxml";
    public static String MAIN_PANE = "/fxml/MainContainer.fxml";
    public static String LOGIN_SCREEN = "/fxml/LoginWindow.fxml";


    private static SaleTabController saleTabController;
    private static MainPaneController mainPaneController;

    /**
     * Stores the main controller for later use in navigation tasks.
     *
     * @param mainController the main application layout controller.
     */
    public static void setMainController(MainPaneController mainController) {
        ScreenChanger.mainPaneController = mainController;
    }

    public static void loadScreen(String url) {
        FXMLLoader loader = new FXMLLoader(ScreenChanger.class.getResource(url));
        try {
            mainPaneController.setScreen(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
