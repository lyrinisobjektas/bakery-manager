package kea.pmm.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import kea.pmm.utility.ObjectLoader;

public class TabPaneController {

    @FXML
    private TabPane tabPane;

    public void initialize() {

        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue.getId());
            if (newValue.getId().equals("historyTab")) {
                refreshOrderHistory();
            }
        });

        System.out.println("test");

    }

    private void refreshOrderHistory() {
        ObjectLoader.loadOrdersFromDb();
    }

}
