package kea.pmm.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import kea.pmm.entity.Employee;
import kea.pmm.utility.ObjectLoader;

import java.io.IOException;

public class MainPaneController {

    @FXML
    private BorderPane borderPane;

    @FXML
    private Button logoutButton;

    @FXML
    private Label userNameLabel;

    private Pane loginScreen;

    public void initialize() {

        borderPane.getBottom().setVisible(false);

        ObjectLoader.currentEmployeeProperty().addListener((observable, oldValue, newValue) -> {
            setEmployeeToolbar(ObjectLoader.getCurrentEmployee());
        });

        try {
            borderPane.setCenter(loadLoginScreen());
        } catch (IOException e) {
            e.printStackTrace();
        }

        logoutButton.setOnAction(event -> {
            borderPane.setCenter(loginScreen);
            ObjectLoader.setCurrentEmployee(null);
            setEmployeeToolbar(ObjectLoader.getCurrentEmployee());
            borderPane.getBottom().setVisible(false);
        });

        ScreenChanger.setMainController(this);

    }

    private void setEmployeeToolbar(Employee currentEmployee) {
        if (currentEmployee == null) {
            userNameLabel.setText("");
        } else {
            userNameLabel.setText(ObjectLoader.getCurrentEmployee().getName());
        }
    }

    private Pane loadLoginScreen() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(ScreenChanger.LOGIN_SCREEN));
        Pane loginScreen = loader.load();
        LoginController loginController = loader.getController();
        loginController.setBorderPane(this.borderPane);
        this.loginScreen = loginScreen;
        return loginScreen;
    }

    public void setScreen(Node node) {
        borderPane.setCenter(node);
    }

}
