package kea.pmm.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Locale;

public class DashboardTabController {

    @FXML
    private BarChart<Integer, Integer> salesChart;

    private ObservableList<String> monthNames = FXCollections.observableArrayList();

    public void initialize() {

        // Get an array with the English month names.
        String[] months = DateFormatSymbols.getInstance(Locale.ENGLISH).getMonths();
        // Convert it to a list and add it to our ObservableList of months.
        monthNames.addAll(Arrays.asList(months));

//        XYChart.Series<Integer, Integer> series = new XYChart.Series<>();
//
//        for (int i = 0; i < 31; i++) {
//            series.getData().add(new XYChart.Data<>(2, i*2));
//        }
//
//        salesChart.getData().add(series);

    }


}
