package kea.pmm.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import kea.pmm.entity.Order;
import kea.pmm.utility.ObjectLoader;
import kea.pmm.utility.ReceiptWriter;
import kea.pmm.utility.XmlReceiptWriter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class HistoryTabController {

    @FXML
    private TableView<Order> orderHistoryTable;

    @FXML
    private TableColumn<Order, Number> orderNoTableCol;

    @FXML
    private TableColumn<Order, String> employeeTableCol;

    @FXML
    private TableColumn<Order, LocalDateTime> dateTableCol;

    @FXML
    private TableColumn<Order, BigDecimal> costTableCol;

    @FXML
    private TableColumn<Order, String> paymentTypeTableCol;

    @FXML
    private Button printReceiptButton;

    @FXML
    private ChoiceBox<?> monthPicker;

    private Order selectedOrder;

    public void initialize() {

        orderHistoryTable.setItems(ObjectLoader.getOrderList());
        orderNoTableCol.setCellValueFactory(param -> param.getValue().orderIdProperty());
        employeeTableCol.setCellValueFactory(param -> param.getValue().getEmployee().nameProperty());
        dateTableCol.setCellValueFactory(param -> param.getValue().dateProperty());
        costTableCol.setCellValueFactory(param -> param.getValue().totalPriceProperty());

        orderHistoryTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.selectedOrder = newValue;
        });

        printReceiptButton.setOnAction(event -> {
            try {
                XmlReceiptWriter.saveToXml(selectedOrder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

}
