package kea.pmm.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.NumberStringConverter;
import kea.pmm.dao.OrderDao;
import kea.pmm.entity.Order;
import kea.pmm.entity.OrderItem;
import kea.pmm.entity.ProductDescription;
import kea.pmm.utility.ObjectLoader;
import kea.pmm.utility.ProductListCell;

import java.io.IOException;

public class SaleTabController {

    @FXML
    private AnchorPane saleTabHolder;

    @FXML
    private Label orderIdLabel;

    @FXML
    private Label totalCostLabel;

    @FXML
    private ListView<ProductDescription> productListView;

    @FXML
    private TableView<OrderItem> orderItemTableView;

    @FXML
    private TableColumn<OrderItem, String> nameTableCol;

    @FXML
    private TableColumn<OrderItem, Number> quantityTableCol;

    @FXML
    private TableColumn<OrderItem, Number> costTableCol;

    @FXML
    private TableColumn<OrderItem, Object> colorTableCol;

    @FXML
    private TableColumn<OrderItem, Object> sizeTableCol;

    @FXML
    private Button payButton;

    @FXML
    private Button clearButton;

    @FXML
    private Button removeItemButton;

    @FXML
    private TextField filterTextField;


    private ObservableList<ProductDescription> productDescriptions;
    private ProductDescription selectedProductDescription;
    private ObjectProperty<Order> currentOrder = new SimpleObjectProperty<>();

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    public void initialize() {

        productDescriptions = ObjectLoader.getProductDescriptions();
        setNewOrder();
        resetView();

        ////////// BUTTONS //////////

        clearButton.setOnAction(event1 -> {
            currentOrder.get().getOrderItems().clear();
        });

        payButton.setOnAction(event -> {
            if (currentOrder.get().getOrderItems().size() == 0) {
                System.out.println("No items in the current order");
            } else {
                loadPayScreen();
            }
        });

        removeItemButton.setOnAction(event1 -> {
            if (orderItemTableView.getSelectionModel().getSelectedItem() == null) {
                System.out.println("please select an item to remove");
            } else {
                currentOrder.get().getOrderItems().remove(orderItemTableView.getSelectionModel().getSelectedItem());
            }
        });

        /**
         * Double click action on the selected product brings a pop up window
         */
        productListView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                createInfoWindow(productListView.getSelectionModel().getSelectedItem());
            }
        });


        ////////////////////////////// LISTS //////////////////////////////

        nameTableCol.setCellValueFactory(cellData -> cellData.getValue().getProductId().getProductDescription().nameProperty());
        colorTableCol.setCellValueFactory(cellData -> cellData.getValue().getProductId().colorProperty());
        sizeTableCol.setCellValueFactory(cellData -> cellData.getValue().getProductId().sizeProperty());
        quantityTableCol.setCellValueFactory(x -> x.getValue().quantityProperty());
        costTableCol.setCellValueFactory(x -> x.getValue().priceProperty());

        // Sets custom cell view. Somehow introduces bugs to filtered search
//        productListView.setCellFactory(a -> new ProductListCell());

        ////////////    search field   /////////////
        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<ProductDescription> filteredData = new FilteredList<>(productDescriptions, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(productDescription -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (productDescription.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(productDescription.getProductDescriptionId()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
            });
        });
        // 5. Add sorted (and filtered) data to the table.
        productListView.setItems(filteredData);

        ///////////////////////////// LISTENERS ////////////////////////////

        /**
         * Sets selectedProductDescription to what is selected in the list view.
         */
        productListView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    this.selectedProductDescription = newValue;
                    System.out.println("selected item: " + newValue);
                });

        currentOrder.addListener((observable, oldValue, newValue) -> {
            System.out.println("order object changed " + oldValue + " to " + newValue);
            resetView();
        });

    }

    public void setNewOrder() {
        Order newOrder = new Order();
        newOrder.setEmployee(ObjectLoader.getCurrentEmployee());
        currentOrder.set(newOrder);
//        OrderDao.saveOrder(currentOrder.get());
    }

    private void resetView() {
        orderItemTableView.setItems(currentOrder.get().getOrderItems());
        totalCostLabel.textProperty().bindBidirectional(currentOrder.get().totalPriceProperty(), new BigDecimalStringConverter());
        orderIdLabel.textProperty().bindBidirectional(currentOrder.get().orderIdProperty(), new NumberStringConverter());
    }

    public void addOrderItem(OrderItem orderItem) {
        System.out.println("about to add orded item");
        currentOrder.get().getOrderItems().add(orderItem);
        System.out.println("order item added");
        // todo bidirectional association requires references on both sides. Could be done in the constructor maybe??
        orderItem.setOrder(currentOrder.get());
//        OrderDao.updateOrder(currentOrder.get());
    }

    public Order getCurrentOrder() {
        return this.currentOrder.get();
    }

    private void createInfoWindow(ProductDescription item) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(ScreenChanger.INFO_WINDOW));
            VBox pane = (VBox) loader.load();

            Stage productInfoStage = new Stage();
            productInfoStage.setTitle(item.getName() + "Info");
//            productInfoStage.initOwner();
            productInfoStage.initModality(Modality.APPLICATION_MODAL);
            productInfoStage.setScene(new Scene(pane));

            ProductInfoController controller = loader.getController();
            controller.setItem(item);
            controller.createMatchingProductList();
            controller.setSaleTabController(this);

            productInfoStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadPayScreen() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/PayScreen.fxml"));
            AnchorPane pane = loader.load();
            PayScreenController controller = loader.getController();
            controller.setCurrentOrder(currentOrder.get());
            controller.setSaleTabController(this);

            Stage stage = new Stage();
            stage.setScene(new Scene(pane));
            stage.setTitle("Payment");
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setScreen(Node node) {
        saleTabHolder.getChildren().setAll(node);
    }

}
