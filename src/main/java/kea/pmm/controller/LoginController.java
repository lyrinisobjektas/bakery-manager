package kea.pmm.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import kea.pmm.entity.Employee;
import kea.pmm.utility.ObjectLoader;

public class LoginController {

    @FXML
    private Button logInButton;

    @FXML
    private TextField usernameField;

    private BorderPane borderPane;

    public void initialize() {

        logInButton.setOnAction(event -> {
            Employee employee = userMatch();
            if (employee != null) {
                ObjectLoader.setCurrentEmployee(employee);
                ScreenChanger.loadScreen(ScreenChanger.TAB_PANE);
                borderPane.getBottom().setVisible(true);
            } else {
                System.out.println("wrong username or password");
            }
        });

    }

    public void setBorderPane(BorderPane borderPane) {
        this.borderPane = borderPane;
    }

    private Employee userMatch() {
        String username = usernameField.getText();
        for (Employee e : ObjectLoader.getEmployees()) {
            if (e.getName().equals(username)) {
                return e;
            }
        }
        return null;
    }

}
