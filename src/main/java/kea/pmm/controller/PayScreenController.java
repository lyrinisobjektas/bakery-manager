package kea.pmm.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import kea.pmm.dao.OrderDao;
import kea.pmm.entity.Order;

import java.io.IOException;

public class PayScreenController {

    SaleTabController saleTabController = new SaleTabController();

    @FXML
    private Button payButton;

    @FXML
    private Button cancelButton;

    @FXML
    private RadioButton cashRadioButton;

    @FXML
    private RadioButton creditRadioButton;

    @FXML
    private ToggleGroup paymentGroup;

    @FXML
    private Label totalCostLabel;


    private Order currentOrder;


    private void initComponents() {

        cashRadioButton.setUserData("Cash");
        creditRadioButton.setUserData("Credit Card");

        payButton.setOnAction(event -> {
            currentOrder.setIsCompleted(true);  // this should revert to false if saving to db fails
            OrderDao.saveOrder(currentOrder);
//            saleTabController.resetView();
            saleTabController.setNewOrder();
            payButton.getScene().getWindow().hide();
        });

        cancelButton.setOnAction(event -> {
            System.out.println(paymentGroup.getSelectedToggle().getUserData());
//            cancelButton.getScene().getWindow().hide();
        });

        totalCostLabel.setText(currentOrder.getTotalPrice().toString());

    }

    private void loadSaleTab() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/PayScreen.fxml"));
            saleTabController.setScreen((AnchorPane)loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentOrder(Order order) {
        this.currentOrder = order;
        initComponents();
    }


    public void setSaleTabController(SaleTabController saleTabController) {
        this.saleTabController = saleTabController;
    }
}
