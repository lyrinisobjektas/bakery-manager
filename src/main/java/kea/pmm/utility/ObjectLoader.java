package kea.pmm.utility;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kea.pmm.dao.*;
import kea.pmm.entity.*;

import java.util.List;

public class ObjectLoader {

    private static ObservableList<ProductDescription> productDescriptions = FXCollections.observableArrayList();
    private static ObservableList<Product> products = FXCollections.observableArrayList();
    private static ObservableList<Size> sizes = FXCollections.observableArrayList();
    private static ObservableList<Color> colors = FXCollections.observableArrayList();
    private static ObservableList<Order> orderList = FXCollections.observableArrayList();
    private static ObservableList<Employee> employees = FXCollections.observableArrayList();

    // This holds the employee object, which is currently using the system
    private static ObjectProperty<Employee> currentEmployee = new SimpleObjectProperty<>();


    public static void loadObjectsFromDb() {
        employees.setAll(EmployeeDao.getEmployees());
        productDescriptions.setAll(ProductDescriptionDao.getProductDescriptions());
        products.setAll(ProductDao.getProducts());
        sizes.setAll(SizeDao.getSizes());
        colors.setAll(ColorDao.getColors());
    }

    public static void loadOrdersFromDb() {
        orderList.setAll(OrderDao.getOrders());
    }

    public static ObservableList<ProductDescription> getProductDescriptions() {
        return productDescriptions;
    }

    public static ObservableList<Product> getProducts() {
        return products;
    }

    public static ObservableList<Size> getSizes() {
        return sizes;
    }

    public static ObservableList<Color> getColors() {
        return colors;
    }

    public static ObservableList<Order> getOrderList() {
        return orderList;
    }

    public static ObservableList<Employee> getEmployees() {
        return employees;
    }

    public static Employee getCurrentEmployee() {
        return currentEmployee.get();
    }

    public static void setCurrentEmployee(Employee currentEmployee) {
        ObjectLoader.currentEmployee.set(currentEmployee);
    }

    public static ObjectProperty<Employee> currentEmployeeProperty() {
        return currentEmployee;
    }
}
