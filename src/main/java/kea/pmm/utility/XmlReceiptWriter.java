package kea.pmm.utility;

import kea.pmm.entity.Order;
import kea.pmm.entity.OrderItem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class XmlReceiptWriter {

    public static void saveToXml(Order order) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("receipt");
            doc.appendChild(rootElement);

            // order id element
            Element orderId = doc.createElement("orderId");
            rootElement.appendChild(orderId);
            orderId.appendChild(doc.createTextNode(Integer.toString(order.getOrderId())));

            // cashier element
            Element cashier = doc.createElement("cashier");
            rootElement.appendChild(cashier);
            cashier.appendChild(doc.createTextNode(Integer.toString(order.getEmployee().getEmployeeId())));

            // date element
            Element purchaseDate = doc.createElement("purchaseDate");
            rootElement.appendChild(purchaseDate);
            Element date = doc.createElement("date");
            purchaseDate.appendChild(date);
            Element time = doc.createElement("time");
            purchaseDate.appendChild(time);
            date.appendChild(doc.createTextNode(order.getDate().toLocalDate().toString()));
            time.appendChild(doc.createTextNode(order.getDate().toLocalTime().toString()));

            // purchase item elements
            Element items = doc.createElement("items");
            rootElement.appendChild(items);

            for (OrderItem item : order.getOrderItems()) {

                Element orderItem = doc.createElement("orderItem");
                Element itemName = doc.createElement("itemName");
                Element itemUnitPrice = doc.createElement("itemUnitPrice");
                Element itemQuantity = doc.createElement("itemQuantity");
                Element itemTotalPrice = doc.createElement("itemTotalPrice");

                itemName.appendChild(doc.createTextNode(item.getProductId().getProductDescription().getName()));
                itemUnitPrice.appendChild(doc.createTextNode(item.getProductId().getPrice().toString()));
                itemQuantity.appendChild(doc.createTextNode(Integer.toString(item.getQuantity())));
                itemTotalPrice.appendChild(doc.createTextNode(item.getPrice().toString()));

                items.appendChild(orderItem);
                orderItem.appendChild(itemName);
                orderItem.appendChild(itemUnitPrice);
                orderItem.appendChild(itemQuantity);
                orderItem.appendChild(itemTotalPrice);
            }

            // total cost element
            Element totalCost = doc.createElement("totalCost");
            rootElement.appendChild(totalCost);
            totalCost.appendChild(doc.createTextNode(order.getTotalPrice().toString()));

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("Receipts/" + order.getOrderId() + ".xml"));

            // Output to console for testing
            StreamResult resultConsole = new StreamResult(System.out);

            // To make indentation in the xml file
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            transformer.transform(source, result);
            // Output to console for testing
            transformer.transform(source, resultConsole);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

}
