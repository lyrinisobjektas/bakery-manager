package kea.pmm.utility;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import kea.pmm.entity.ProductDescription;

/**
 * This class is used for testing purposes. It changes how the list of product descriptions is displayed.
 */

public class ProductListCell extends ListCell<ProductDescription> {

    @Override
    protected void updateItem(ProductDescription item, boolean bln) {
        super.updateItem(item, bln);
        if (item != null) {
            Label nameLabel = new Label();
            nameLabel.setText(item.getName());
            Image img = new Image(item.getImageLink());
//             resizes the image to have width of 100 while preserving the ratio and using
//             higher quality filtering method; this ImageView is also cached to
//             improve performance
            HBox hbox = new HBox();
            ImageView imgView = new ImageView(img);
            imgView.setFitHeight(60);
            imgView.setPreserveRatio(true);
            imgView.setSmooth(true);
            imgView.setCache(true);
            hbox.getChildren().addAll(nameLabel, imgView);
            setGraphic(hbox);
        }
    }


}
