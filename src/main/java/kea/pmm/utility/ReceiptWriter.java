package kea.pmm.utility;

import kea.pmm.entity.Order;
import kea.pmm.entity.OrderItem;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class ReceiptWriter {

    // Create a new font objects selecting one of the PDF base fonts
    private static PDFont HEADING_1 = PDType1Font.HELVETICA_BOLD;
    private static PDFont BODY = PDType1Font.HELVETICA;
    private static String RECEIPTS = "Receipts/";

    public static void printReceipt(Order order) throws Exception {

        // Create a document and add a page to it
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);

        // Start a new content stream which will "hold" the to be created content
        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        // Define a text content stream using the selected font, moving the cursor and drawing the text "Hello World"
        contentStream.beginText();
        contentStream.setFont(HEADING_1, 12);
        contentStream.moveTextPositionByAmount(100, 700);
        contentStream.drawString("Enli5");
        for (OrderItem i : order.getOrderItems()) {
            contentStream.drawString(i.getProductId() + " " + i.getQuantity() + i.getPrice() + "\n");
        }
        contentStream.endText();

        // Make sure that the content stream is closed:
        contentStream.close();

        // Save the results and ensure that the document is properly closed:
        document.save(RECEIPTS +  order.getOrderId() + ".pdf");
        document.close();
    }

}
