package kea.pmm;

import kea.pmm.dao.ProductDescriptionDao;
import kea.pmm.entity.ProductDescription;

import java.util.List;

public class MainTest {

    public static void main(String[] args) {

        List<ProductDescription> productDescriptions;

        productDescriptions = ProductDescriptionDao.getProductDescriptions();

        System.out.println(productDescriptions);

    }
}
