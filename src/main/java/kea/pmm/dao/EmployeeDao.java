package kea.pmm.dao;

import kea.pmm.entity.Employee;
import kea.pmm.utility.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class EmployeeDao {

    public static List<Employee> getEmployees() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Employee> result = session.createQuery("from Employee").list();
        session.getTransaction().commit();
        return result;
    }

}
