package kea.pmm.dao;

import kea.pmm.entity.Product;
import kea.pmm.utility.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class ProductDao {

    public static List<Product> getProducts() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Product> result = session.createQuery("from Product").list();
        session.getTransaction().commit();
        return result;
    }

    public static List<Product> getProductsByDescriptionId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Product> result = session.createQuery("from Product WHERE productDescriptionId=" + id).list();
        session.getTransaction().commit();
        return result;
    }

}
