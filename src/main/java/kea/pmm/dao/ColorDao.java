package kea.pmm.dao;

import kea.pmm.entity.Color;
import kea.pmm.entity.Product;
import kea.pmm.utility.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class ColorDao {

    public static List<Color> getColors() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Color> result = session.createQuery("from Color").list();
        session.getTransaction().commit();
        return result;
    }

}
