package kea.pmm.dao;

import kea.pmm.entity.Order;
import kea.pmm.utility.HibernateUtil;
import org.hibernate.Session;

import java.time.LocalDateTime;
import java.util.List;

public class OrderDao {

    public static void saveOrder(Order order) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        // Updates the date of the order just before saving to the db
        order.setDate(LocalDateTime.now());
        session.persist(order);
        session.getTransaction().commit();
    }

    public static void updateOrder(Order order) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(order);
        session.getTransaction().commit();
    }

    public static List<Order> getOrders() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Order> result = session.createQuery("from Order").list();
        session.getTransaction().commit();
        return result;
    }

}
