package kea.pmm.dao;

import kea.pmm.entity.Product;
import kea.pmm.entity.ProductDescription;
import kea.pmm.utility.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class ProductDescriptionDao {

    public static List<ProductDescription> getProductDescriptions() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<ProductDescription> result = session.createQuery("from ProductDescription").list();
        session.getTransaction().commit();
        return result;
    }

}
