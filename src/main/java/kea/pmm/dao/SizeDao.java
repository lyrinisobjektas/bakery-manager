package kea.pmm.dao;

import kea.pmm.entity.Product;
import kea.pmm.entity.Size;
import kea.pmm.utility.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class SizeDao {

    public static List<Size> getSizes() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Size> result = session.createQuery("from Size").list();
        session.getTransaction().commit();
        return result;
    }

}
